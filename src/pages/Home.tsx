import React, { useState } from 'react';
import { IonContent, IonHeader, IonFooter, IonPage, IonTitle, IonToolbar, IonFab, IonFabButton, IonIcon, IonFabList } from '@ionic/react';
import { camera, downloadOutline, image} from 'ionicons/icons'
import ExploreContainer from '../components/ExploreContainer';
import { Camera, CameraResultType } from '@capacitor/camera';
import { getDownloadURL, getStorage, ref, uploadString } from "firebase/storage";
import './Home.css';
import firebaseApp from '../firebaseConfig';



const Home: React.FC = () => {
  const storage = getStorage(firebaseApp);
  const [image2,setImage2] = useState<string>('');
  const [image3,setImage3] = useState<string>('');
  var img1
 
  const takePicture = async () => {
   try{
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    })
    
    setImage2(image.webPath || '');
    const storageRef = ref(storage,'images/'+image.webPath);
    console.log(image);
    console.log(image.webPath);
    uploadString(storageRef,image.format).then((snapshot) => {
      console.log('Uploaded a blob or file!');
    });
  }catch(error){
    console.log(error);
  }
  };

  const downloadPicture = async ()=>{
    getDownloadURL(ref(storage, 'images/'+image2))
    .then((url) => {
 
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = (event) => {
        const blob = xhr.response;
      };
      xhr.open('GET', url);
      
      xhr.send(url);
      setImage3(url);
    })
    .catch((error) => {
      // Handle any errors
    });
  }
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Subir Foto</IonTitle>
          </IonToolbar>
        </IonHeader>
        { image2 !== ''
          ?<img src={image2} />
          : <p>Toma una foto</p>
        }
        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton onClick={downloadPicture}>
            <IonIcon icon={downloadOutline} />
          </IonFabButton>
          <IonFabButton onClick={takePicture}>
            <IonIcon icon={camera} />
          </IonFabButton>
        </IonFab>
        <a href={image3}>hey</a>
        <ExploreContainer />
      </IonContent>
    </IonPage>
  );
};

export default Home;
