import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';
import { getStorage } from "firebase/storage";

export const firebaseConfig={
  apiKey: "AIzaSyBa3iJWOTo8NJJIKyMj44LnIjf5Ylwx69c",
  authDomain: "appprueba-ef838.firebaseapp.com",
  databaseURL:"gs://appprueba-ef838.appspot.com",
  projectId: "appprueba-ef838",
  storageBucket: "appprueba-ef838.appspot.com",
  messagingSenderId: "451165748466",
  appId: "1:451165748466:web:bda9a1e51a8779ad9de46b",
  measurementId: "G-ZWH55XBS6F"

}

const firebaseApp=initializeApp(firebaseConfig)
const storage = getStorage(firebaseApp);

export default firebaseApp;